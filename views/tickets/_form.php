<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tickets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tickets-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Ticket_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Passenger_name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Flight_id')->textInput() ?>

    <?= $form->field($model, 'Passenger_surname')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Customer_ID')->textInput() ?>

    <?= $form->field($model, 'Passenger_passport_series')->textInput() ?>

    <?= $form->field($model, 'Passenger_passport_number')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
