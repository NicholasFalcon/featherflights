<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tickets */

$this->title = $model->Ticket_no;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tickets-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Ticket_no' => $model->Ticket_no, 'Flight_id' => $model->Flight_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Ticket_no' => $model->Ticket_no, 'Flight_id' => $model->Flight_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Ticket_no',
            'Passenger_name:ntext',
            'Flight_id',
            'Passenger_surname:ntext',
            'Customer_ID',
            'Passenger_passport_series',
            'Passenger_passport_number',
        ],
    ]) ?>

</div>
