<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SearchTickets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tickets-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Ticket_no') ?>

    <?= $form->field($model, 'Passenger_name') ?>

    <?= $form->field($model, 'Flight_id') ?>

    <?= $form->field($model, 'Passenger_surname') ?>

    <?= $form->field($model, 'Customer_ID') ?>

    <?php // echo $form->field($model, 'Passenger_passport_series') ?>

    <?php // echo $form->field($model, 'Passenger_passport_number') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
