<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tickets */

$this->title = 'Update Tickets: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Ticket_no, 'url' => ['view', 'Ticket_no' => $model->Ticket_no, 'Flight_id' => $model->Flight_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tickets-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
