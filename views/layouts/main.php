<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="header">
    <div id="MainHeader">
        <div id="headerInner">
            <div class="logo">
                <a href="#"><img src="images/logo.png" alt="FeatherFlights" /></a>
            </div>
        </div>
    </div>
    <div id="MainMenu">
        <?php
        NavBar::begin([
            'options' => [
                'class' => 'navbar-inverse',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'MainMenu'],
            'items' => [
                !Yii::$app->user->isGuest ? (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Выход (' . Yii::$app->user->identity->email . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                ) : (''),
                Yii::$app->user->isGuest ? (
                    ['label' => 'Личный кабинет', 'url' => ['/site/login']]
                ) : (
                    ['label' => 'Личный кабинет', 'url' => ['/site/personal']]
                ),
                ['label' => 'Поиск билетов', 'url' => ['/site/index']]
            ],
        ]);
        NavBar::end();
        ?>
    </div>
    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
