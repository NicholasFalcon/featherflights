<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Регистрация';
?>
<div class="site-signup">
   <h1><?= Html::encode($this->title) ?></h1>
    <p>Заполните данные для регистрации на сайте</p> 
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
        <?= $form->field($model, 'Name')->textInput(['autofocus' =>true])->label('Имя') ?>
        <?= $form->field($model, 'Surname')->label('Фамилия') ?>
<?= $form->field($model, 'DateOfBirth')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '9999-99-99',])->label('Дата рождения') ?>
        <?= $form->field($model, 'email')->label('Электронная почта') ?>
        <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>
        <?= $form->field($model, 'verifyCode')->widget(Captcha::className())->label('Код проверки') ?>
    <p>Регистрируясь на нашем сайте, вы подтверждаете <a href="Polzovatelskoe_soglashenie.docx">соглашение</a> на обработку персональных данных</p>
        <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end(); ?>
</div>
