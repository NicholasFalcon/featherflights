<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $f = ActiveForm::begin(); ?>
	<?=$f->field($form, 'name')?>
	<?=$f->field($form, 'password')->passwordInput(); ?>
	<?= Html::submitButton('Войти'); ?>
<?php ActiveForm::end(); ?>
