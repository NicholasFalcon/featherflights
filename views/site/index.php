<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'FeatherFlights';
?>
<div id="wrapper" background="/images/background-menu.png">
    <div id="MainUp">
        <nav class="MainUpMenu">
 
            <ul>
                <li><a href="#">Покупка</a></li>
                <li><a href="#">Регистрация на рейс</a></li>
                <li><a href="#">Статус рейса</a></li>
            </ul>
     
        </nav>
    </div>
    <?php $form = ActiveForm::begin([
        'id'=>'middle', ]); ?>
    <div id="MidUpper">
        <ul>
            <li>
                <a>
                    <input placeholder="Откуда" type="text" id="searchfrom-townwhere" name="SearchForm[townWhere]" list="ListCity">
                    <input placeholder="Куда" type="text" id="searchfrom-townfrom" name="SearchForm[townWhere]" list="ListCity">
                    <datalist id="ListCity">
                        <option>Москва</option>
                        <option>Санкт-Петербург</option>
                    </datalist>
                </a>
            </li>
            <li>
                <a>
                    <input placeholder="Дата отправления" class="DateDeparture" type="text" onfocus="(this.type='date')" onblur="(this.type='text')"  id="InputDateDeparture" name="SearchForm[dateWhere]"> 
                    <input placeholder="Дата возвращения" class="DateReturn" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" id="InputDateReturn" name="SearchForm[dateFrom]">
                </a>
            </li>
        </ul>
    </div>
    <div id="MidDowner">
        <ul>
            <li>
                    <input type="radio" id="tuda" name='RadioKuda' onchange="scriptForSite()">
                    <input type="radio" id="obratno" name='RadioKuda' onchange="scriptForSite()">
                    <label class="LabTuda" for="tuda">Только туда</label>
                    <label class="LabObratno" for="obratno">Туда и обратно</label>
                    <button type="submit" id="FindSubmitButton" class="buttonFind" name="search-button">Найти</button>
                    <label class="ButtonFind" for="FindSubmitButton">Найти</label>
            </li>
        </ul>
    </div>
    <?php ActiveForm::end(); ?>
</div>
