--
-- PostgreSQL database dump
--

-- Dumped from database version 10.2
-- Dumped by pg_dump version 10.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = public, pg_catalog;

--
-- Name: add_customer(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_customer(uemail text, new_pswd text) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  BEGIN
    insert into customers(email, password) values(uemail, crypt(new_pswd, gen_salt('md5')));
  END;
  
  $$;


ALTER FUNCTION public.add_customer(uemail text, new_pswd text) OWNER TO postgres;

--
-- Name: add_user(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_user() RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
  BEGIN
    execute 'create user ' || login || ' with password ''' || pswd  || ''' in role air_user;';
  END;
  
  $$;


ALTER FUNCTION public.add_user() OWNER TO postgres;

--
-- Name: add_user(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_user(login text, pswd text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  BEGIN
    execute 'create user ' || login || ' with password ''' || pswd  || ''' in role air_user;';
  END;
  
  $$;


ALTER FUNCTION public.add_user(login text, pswd text) OWNER TO postgres;

--
-- Name: available_flights(text, text, date, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION available_flights(cityf text, cityt text, ftime date, fare_cond text) RETURNS TABLE(flight_id bigint, dep_time time without time zone, arr_time time without time zone, arrival_airport character, departure_airport character, aircraft_code text, price numeric)
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
declare
  hi int;
begin
  return query
  select f.flight_id, f.departure_time::time as dep_time,
  f.arrival_time::time as arr_time,
  f.arrival_airport, f.departure_airport, a3.aircraft_model,
  p.price
  from flights as f 
  inner join airports as a1 on a1.airport_code = f.arrival_airport 
  inner join airports as a2 on a2.airport_code = f.departure_airport
  inner join prices as p on f.flight_id = p.flight_id
  inner join aircrafts as a3 on a3.aircraft_code = f.aircraft_code
  where a1.city = cityT and a2.city = cityF and f.departure_time::date = ftime and (tickets_left(f.flight_id, fare_cond) > 0) and p.fare_condition = fare_cond;
 
end;
$$;


ALTER FUNCTION public.available_flights(cityf text, cityt text, ftime date, fare_cond text) OWNER TO postgres;

--
-- Name: buy_ticket(bigint, integer, integer, text, text, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION buy_ticket(nflight_id bigint, npassport_series integer, npassport_number integer, npassenger_name text, npassenger_surname text, nprice integer, nfare_condition text) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
  declare 
  u text;
  
begin
  select session_user into u;
  insert into tickets values(generate_ticket_num(), npassport_series, npassenger_name, nprice, npassenger_surname, npassport_number, nflight_id, nfare_condition, u);
end;
$$;


ALTER FUNCTION public.buy_ticket(nflight_id bigint, npassport_series integer, npassport_number integer, npassenger_name text, npassenger_surname text, nprice integer, nfare_condition text) OWNER TO postgres;

--
-- Name: buy_ticket(bigint, integer, integer, text, character varying, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION buy_ticket(nflight_id bigint, npassport_series integer, npassport_number integer, npassenger_name text, npassenger_surname character varying, nprice integer, nfare_condition text) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
  declare 
  u text;
  
begin
  select session_user into u;
  insert into tickets values(generate_ticket_num(), npassport_series, npassenger_name, nprice, npassenger_surname, npassport_number, nflight_id, nfare_condition, u);
end;
$$;


ALTER FUNCTION public.buy_ticket(nflight_id bigint, npassport_series integer, npassport_number integer, npassenger_name text, npassenger_surname character varying, nprice integer, nfare_condition text) OWNER TO postgres;

--
-- Name: check_admin(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION check_admin() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    b bool;
  BEGIN
  b = false;
    
    select pg_has_role('air_admin', 'member') into b;
    return b;
  END;
  
  $$;


ALTER FUNCTION public.check_admin() OWNER TO postgres;

--
-- Name: check_in_for_flight(character varying, bigint, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION check_in_for_flight(seat_n character varying, fl_id bigint, tick_no character) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
  begin
  insert into boarding_passes(seat_no, flight_id, ticket_no) values(seat_n, fl_id, tick_no);
  end;
  $$;


ALTER FUNCTION public.check_in_for_flight(seat_n character varying, fl_id bigint, tick_no character) OWNER TO postgres;

--
-- Name: check_pswd(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION check_pswd(uemail text, pswd text) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    b bool;
  BEGIN
    
    b = (select password = crypt(pswd, password) from customers where email = uemail);
    return b;
  END;
  
  $$;


ALTER FUNCTION public.check_pswd(uemail text, pswd text) OWNER TO postgres;

--
-- Name: flight_status_by_id(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION flight_status_by_id(fl_id bigint) RETURNS TABLE(flight_id bigint, status character varying, dep_time time without time zone, arr_time time without time zone, departure_airport character, arrival_airport character, aircraft_code character)
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
  declare 

begin
  return query
  select f.flight_id, f.status, f.departure_time::time as dep_time,
  f.arrival_time::time as arr_time, 
  f.departure_airport,
  f.arrival_airport, 
  f.aircraft_code
  from flights as f 
  where f.flight_id = fl_id;
end;
$$;


ALTER FUNCTION public.flight_status_by_id(fl_id bigint) OWNER TO postgres;

--
-- Name: flight_status_by_route(text, text, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION flight_status_by_route(cityf text, cityt text, ftime date) RETURNS TABLE(flight_id bigint, status character varying, dep_time time without time zone, arr_time time without time zone, departure_airport character, arrival_airport character, aircraft_code character)
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
  declare 

begin
  return query
  select f.flight_id, f.status, f.departure_time::time as dep_time,
  f.arrival_time::time as arr_time, 
  f.departure_airport,
  f.arrival_airport, 
  f.aircraft_code
  from flights as f 
  inner join airports as a1 on a1.airport_code = f.arrival_airport 
  inner join airports as a2 on a2.airport_code = f.departure_airport
  where a1.city = cityT and a2.city = cityF and f.departure_time::date = ftime;
end;
$$;


ALTER FUNCTION public.flight_status_by_route(cityf text, cityt text, ftime date) OWNER TO postgres;

--
-- Name: generate_ticket_num(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION generate_ticket_num() RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
  t text;
  num character(13);
  BEGIN
    loop
    num = concat(to_char(trunc(random()*10),'FM9') , to_char(trunc(random()*10),'FM9') , to_char(trunc(random()*10),'FM9'), to_char(trunc(random()*10),'FM9'), to_char(trunc(random()*10),'FM9'), to_char(trunc(random()*10),'FM9'), to_char(trunc(random()*10),'FM9'), to_char(trunc(random()*10),'FM9'), to_char(trunc(random()*10),'FM9'), to_char(trunc(random()*10),'FM9'), to_char(trunc(random()*10),'FM9'), to_char(trunc(random()*10),'FM9'), to_char(trunc(random()*10),'FM9'));
    exit when not exists(select * from tickets where ticket_no = num);
    end loop;
    return num;
  END;
  
  $$;


ALTER FUNCTION public.generate_ticket_num() OWNER TO postgres;

--
-- Name: info_for_boarding_pass(character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION info_for_boarding_pass(tick_no character) RETURNS TABLE(cityf text, cityt text, datetimedep timestamp without time zone, datetimearr timestamp without time zone, airportf character, airportt character, seatt character varying, f_id bigint, pas_surn text, pas_name text, b_n integer)
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
begin
  return query
  select a1.city as cityF, 
  a2.city as cityT,
  f.departure_time as datetimedep,
  f.arrival_time as datetimearr,
  a1.airport_code as airportF, 
  a2.airport_code as airportT,
  bp.seat_no, f.flight_id,
  t.passenger_surname, t.passenger_name,
  bp.boarding_no
  from tickets as t
  inner join flights as f on t.flight_id = f.flight_id
  inner join airports as a1 on a1.airport_code = f.arrival_airport 
  inner join airports as a2 on a2.airport_code = f.departure_airport
  inner join aircrafts as a3 on a3.aircraft_code = f.aircraft_code 
  inner join boarding_passes as bp on bp.ticket_no = t.ticket_no 
  where t.ticket_no = tick_no;
  end;
$$;


ALTER FUNCTION public.info_for_boarding_pass(tick_no character) OWNER TO postgres;

--
-- Name: info_for_ticket(character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION info_for_ticket(tick_no character) RETURNS TABLE(cityf text, cityt text, datetimedep timestamp without time zone, datetimearr timestamp without time zone, travel_time time without time zone, airportf text, airportt text, aircraft_model text)
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
begin
  return query
  select a1.city as cityF, 
  a2.city as cityT,
  f.departure_time as datetimedep,
  f.arrival_time as datetimearr,
  (f.arrival_time - f.departure_time)::time as travel_time,
  concat(a1.airport_name, ' ', a1.airport_code) as airportF, 
  concat(a2.airport_name, ' ', a2.airport_code) as airportT,
  a3.aircraft_model
  from tickets as t
  inner join flights as f on t.flight_id = f.flight_id
  inner join airports as a1 on a1.airport_code = f.arrival_airport 
  inner join airports as a2 on a2.airport_code = f.departure_airport
  inner join aircrafts as a3 on a3.aircraft_code = f.aircraft_code  
  where t.ticket_no = tick_no;
  end;
$$;


ALTER FUNCTION public.info_for_ticket(tick_no character) OWNER TO postgres;

--
-- Name: reg(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION reg() RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
  declare 
  cur CURSOR for select seat_no from boarding_passes 
    order by CAST(nullif(left(seat_no, LENGTH(seat_no)-1), '') AS integer), right(seat_no, 1) ASC;
  _seat_no varchar;
  _count int;
begin
  _count = 0;
  open cur;
  loop
  fetch cur into _seat_no;
  IF NOT FOUND THEN EXIT;END IF;
  _count = _count + 1;
  end loop;
  close cur;
  return _count;
end;
$$;


ALTER FUNCTION public.reg() OWNER TO postgres;

--
-- Name: reg(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION reg(pass_count integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
  declare 
  cur CURSOR for select seat_no from tickets  
  inner join flights on flight_id = flight_id
  inner join aircrafts on aicraft_code = aircraft_code
  inner join seats on seat_no = seat_no where fare_condition = _fc;
  _seat_no varchar;
  _count int;
  arr varchar[];
begin
  
  open cur;
  loop
  fetch cur into _seat_no;
  if not exists(select seat_no from boarding_passes where seat_no = _seat_no)
  then
  IF NOT FOUND THEN EXIT;END IF;
    loop 
    
    exit when _count = pass_count;
    end loop;
  end if;

  end loop;
  close cur;
  return _count;
end;
$$;


ALTER FUNCTION public.reg(pass_count integer) OWNER TO postgres;

--
-- Name: reg(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION reg(_ticket_no character varying) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
  declare 
  _fc varchar;
  cur CURSOR for select seats.seat_no, flights.flight_id from tickets inner join flights on tickets.flight_id = flights.flight_id
  inner join aircrafts on flights.aircraft_code = aircrafts.aircraft_code
  inner join seats on seat_no = seat_no where ticket_no = _ticket_no and fare_condition = _fc;
  _seat_no varchar;
  _count int;
  _fl_id int;
begin
  select into _fc fare_condition from tickets where ticket_no = _ticket_no;
  open cur;
  loop
  fetch cur into _seat_no, _fl_id;
  IF NOT FOUND THEN EXIT;END IF;
  if not exists(select seat_no from boarding_passes where seat_no = _seat_no)
  then
  insert into boarding_passes(seat_no, flight_id, ticket_no) values(_seat_no, _fl_id, _ticket_no);
  exit;
  end if;
  end loop;
  close cur;
end;
$$;


ALTER FUNCTION public.reg(_ticket_no character varying) OWNER TO postgres;

--
-- Name: seats_for_boarding(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION seats_for_boarding(fl_id bigint) RETURNS TABLE(seat_no character varying, free boolean, fare_condition text)
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
    begin
    return query
    select s.seat_no, not exists (select boarding_passes.seat_no from boarding_passes where boarding_passes.seat_no = s.seat_no and boarding_passes.flight_id = fl_id) free, s.fare_condition free from flights as f
    inner join seats as s on f.aircraft_code = s.aircraft_code
    where f.flight_id = fl_id;
    end;
    $$;


ALTER FUNCTION public.seats_for_boarding(fl_id bigint) OWNER TO postgres;

--
-- Name: select_user_tickets(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION select_user_tickets() RETURNS TABLE(ticket_no character, flight_id bigint, passport_series integer, passport_number integer, passenger_name text, passenger_surname text, fare_condition text, price integer)
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
begin
  return query
  select tickets.ticket_no, tickets.flight_id, tickets.passport_series, tickets.passport_number, tickets.passenger_name , tickets.passenger_surname  , tickets.fare_condition , tickets.price from tickets
  where tickets.user = session_user; 
end;
$$;


ALTER FUNCTION public.select_user_tickets() OWNER TO postgres;

--
-- Name: tick_has_bp(character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION tick_has_bp(tick_no character, OUT b boolean) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
begin
  if not exists(select tickets.ticket_no from tickets inner join boarding_passes on tickets.ticket_no = boarding_passes.ticket_no where tickets.ticket_no = tick_no)
  then b = false;
  else b = true;
  end if;
  end;
$$;


ALTER FUNCTION public.tick_has_bp(tick_no character, OUT b boolean) OWNER TO postgres;

--
-- Name: tickets_left(bigint, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION tickets_left(nflight_id bigint, nfare_condition text, OUT n_value integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
declare 
  b_tickets int;
  all_tickets int;
begin
  if (nfare_condition not in ('Бизнес', 'Эконом')) then

  select count(*) into b_tickets from tickets
  inner join flights on tickets.flight_id = flights.flight_id
  where flights.flight_id = nflight_id;
  
  select count(*) into all_tickets from aircrafts
  inner join flights on flights.aircraft_code = aircrafts.aircraft_code 
  inner join seats on aircrafts.aircraft_code = seats.aircraft_code
  WHERE flights.flight_id = nflight_id;
  else

  select count(*) into b_tickets from tickets
  inner join flights on tickets.flight_id = flights.flight_id
  where flights.flight_id = nflight_id and tickets.fare_condition = nfare_condition;
  
  select count(*) into all_tickets from aircrafts
  inner join flights on flights.aircraft_code = aircrafts.aircraft_code 
  inner join seats on aircrafts.aircraft_code = seats.aircraft_code
  WHERE flights.flight_id = nflight_id and seats.fare_condition = nfare_condition;
  end if;
  n_value = all_tickets - b_tickets;  
end;
$$;


ALTER FUNCTION public.tickets_left(nflight_id bigint, nfare_condition text, OUT n_value integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aircrafts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE Aircrafts (
    aircraft_code character(3) NOT NULL,
    aircraft_model text NOT NULL
);


ALTER TABLE aircrafts OWNER TO postgres;

--
-- Name: airports; Type: TABLE; Schema: public; Owner: postgres
--

create table customers (
    customer_id serial,
    name varchar(30),
    surname varchar(30),
    email text,
    pswd text,
    date_of_birth date,
    access_token text,
    auth_key text
);

CREATE TABLE Airports (
    airport_code character(3) NOT NULL,
    airport_name text NOT NULL,
    city text NOT NULL,
    timezone text NOT NULL,
    country text NOT NULL
);


ALTER TABLE airports OWNER TO postgres;

--
-- Name: boarding_passes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE boarding_passes (
    boarding_no integer NOT NULL,
    seat_no character varying(4) NOT NULL,
    flight_id bigint NOT NULL,
    ticket_no character(13) NOT NULL
);


ALTER TABLE boarding_passes OWNER TO postgres;

--
-- Name: boarding_passes_boarding_no_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE boarding_passes_boarding_no_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE boarding_passes_boarding_no_seq OWNER TO postgres;

--
-- Name: boarding_passes_boarding_no_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE boarding_passes_boarding_no_seq OWNED BY boarding_passes.boarding_no;


--
-- Name: flights; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE Flights (
    flight_id bigint NOT NULL,
    flight_no character(6) NOT NULL,
    departure_time timestamp without time zone NOT NULL,
    arrival_time timestamp without time zone NOT NULL,
    status character varying(20) NOT NULL,
    arrival_airport character(3) NOT NULL,
    departure_airport character(3) NOT NULL,
    aircraft_code character(3) NOT NULL,
    CONSTRAINT flights_check CHECK ((arrival_time > departure_time))
);


ALTER TABLE flights OWNER TO postgres;

--
-- Name: flights_flight_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE flights_flight_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE flights_flight_id_seq OWNER TO postgres;

--
-- Name: flights_flight_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE flights_flight_id_seq OWNED BY flights.flight_id;


--
-- Name: prices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE Prices (
    price_id integer NOT NULL,
    flight_id bigint NOT NULL,
    fare_condition text NOT NULL,
    price numeric NOT NULL,
    CONSTRAINT prices_price_check CHECK (((price)::double precision >= (0)::double precision))
);


ALTER TABLE prices OWNER TO postgres;

--
-- Name: prices_price_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE prices_price_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE prices_price_id_seq OWNER TO postgres;

--
-- Name: prices_price_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE prices_price_id_seq OWNED BY prices.price_id;


--
-- Name: seats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE Seats (
    seat_no character varying(3) NOT NULL,
    fare_condition text NOT NULL,
    aircraft_code character(3) NOT NULL
);


ALTER TABLE seats OWNER TO postgres;

--
-- Name: tickets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE Tickets (
    ticket_no character(13) NOT NULL,
    passport_series integer NOT NULL,
    passenger_name text NOT NULL,
    price integer NOT NULL,
    passenger_surname text NOT NULL,
    passport_number integer NOT NULL,
    flight_id bigint NOT NULL,
    fare_condition text,
    customer_id bigint
);


ALTER TABLE tickets OWNER TO postgres;

--
-- Name: boarding_passes boarding_no; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY boarding_passes ALTER COLUMN boarding_no SET DEFAULT nextval('boarding_passes_boarding_no_seq'::regclass);


--
-- Name: flights flight_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flights ALTER COLUMN flight_id SET DEFAULT nextval('flights_flight_id_seq'::regclass);


--
-- Name: prices price_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY prices ALTER COLUMN price_id SET DEFAULT nextval('prices_price_id_seq'::regclass);


--
-- Data for Name: aircrafts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aircrafts (aircraft_code, aircraft_model) FROM stdin;
717	Boeing 717
70F	Boeing 707 Freighter
70M	Boeing 707 Combi
321	213
\.


--
-- Data for Name: airports; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY airports (airport_code, airport_name, city, timezone, country) FROM stdin;
AAQ	Анапа	Анапа (Витязево)	Europe/Moscow	Россия
EGO	Белгород	Белгород	Europe/Moscow	Россия
VOZ	Чертовицкое	Воронеж	Europe/Moscow	Россия
VKO	Внуково	Москва	Europe/Moscow	Россия
DME	Домодедово	Москва	Europe/Moscow	Россия
OSF	Остафьево	Москва	Europe/Moscow	Россия
SVO	Шереметьево	Москва	Europe/Moscow	Россия
LED	Пулково	Санкт-Петербург	Europe/Moscow	Россия
VOG	Гумрак	Волгоград	Europe/Moscow	Россия
ABA	Абакан	Абакан	Asia/Krasnoyarsk	Россия
ULV	Баратаевка	Ульяновск	Europe/Ulyanovsk	Россия
\.


--
-- Data for Name: boarding_passes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY boarding_passes (boarding_no, seat_no, flight_id, ticket_no) FROM stdin;
49	2A	10	8261942982228
50	11E	13	2751006073200
51	8B	13	3199334703013
\.


--
-- Data for Name: flights; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY flights (flight_id, flight_no, departure_time, arrival_time, status, arrival_airport, departure_airport, aircraft_code) FROM stdin;
4	000004	2018-05-09 02:20:34.78771	2018-05-09 12:00:00	Приземлился	VOZ	ABA	717
5	000005	2018-06-02 05:12:00.086	2018-06-02 13:12:53.09	Отложен	DME	LED	70F
8	000008	2018-06-02 05:12:00.086	2018-06-02 13:12:53.09	По расписанию	VOZ	OSF	717
9	000009	2018-06-02 05:12:00.086	2018-06-02 13:12:53.09	По расписанию	LED	ABA	717
10	000010	2018-06-02 05:12:00.086	2018-06-02 13:12:53.09	По расписанию	ULV	DME	717
11	000011	2018-06-02 05:12:00.086	2018-06-02 13:12:53.09	По расписанию	OSF	ULV	717
1	000001	2018-04-16 23:20:34.787713	2018-04-17 00:00:00	Приземлился	DME	ABA	717
2	000002	2018-04-17 23:25:06.973652	2018-04-18 00:00:00	Приземлился	SVO	OSF	717
6	000006	2018-06-02 05:12:00.086	2018-06-02 13:12:53.09	По расписанию	SVO	AAQ	717
12	000012	2018-06-02 05:12:00.086	2018-06-02 13:12:53.09	По расписанию	EGO	OSF	70F
7	000007	2018-06-02 05:12:00.086	2018-06-02 13:12:53.09	По расписанию	VOZ	ABA	70F
3	000001	2018-05-26 23:20:34.787	2018-05-27 00:00:00	Приземлился	DME	ABA	70M
13	000014	2018-06-02 08:33:29.75	2018-06-02 12:00:29.75	По расписанию	DME	ULV	70F
\.


--
-- Data for Name: prices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY prices (price_id, flight_id, fare_condition, price) FROM stdin;
1	1	Эконом	12350
3	2	Бизнес	10360.60
2	1	Бизнес	23360.30
4	2	Эконом	5400
5	3	Эконом	7350
6	3	Бизнес	21988
7	4	Эконом	12500
8	4	Бизнес	20700
10	5	Эконом	40879
11	5	Бизнес	59800
12	6	Бизнес	16004
13	6	Эконом	7800
14	7	Эконом	21456
15	7	Бизнес	31244
16	8	Эконом	12345
17	8	Бизнес	22352
18	9	Эконом	3450
19	9	Бизнес	4500
20	10	Бизнес	12563
21	10	Эконом	7820
22	11	Эконом	5500
23	11	Бизнес	7600
24	12	Эконом	17400
25	12	Бизнес	23900
27	13	Бизнес	12670
26	13	Эконом	10300
\.


--
-- Data for Name: seats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY seats (seat_no, fare_condition, aircraft_code) FROM stdin;
3A	Эконом	717
3B	Эконом	717
3C	Эконом	717
3D	Эконом	717
3E	Эконом	717
3F	Эконом	717
1A	Бизнес	717
1B	Бизнес	717
2A	Бизнес	717
2B	Бизнес	717
4A	Эконом	717
4B	Эконом	717
4C	Эконом	717
4D	Эконом	717
4E	Эконом	717
4F	Эконом	717
5A	Эконом	717
5B	Эконом	717
5C	Эконом	717
5D	Эконом	717
5E	Эконом	717
5F	Эконом	717
6A	Эконом	717
6B	Эконом	717
6C	Эконом	717
6D	Эконом	717
6E	Эконом	717
6F	Эконом	717
7A	Эконом	717
7B	Эконом	717
7C	Эконом	717
7D	Эконом	717
10A	Эконом	717
10B	Эконом	717
10C	Эконом	717
10D	Эконом	717
10E	Эконом	717
10F	Эконом	717
7E	Эконом	717
7F	Эконом	717
8A	Эконом	717
8B	Эконом	717
8C	Эконом	717
8D	Эконом	717
8E	Эконом	717
8F	Эконом	717
9A	Эконом	717
9B	Эконом	717
9C	Эконом	717
9D	Эконом	717
9E	Эконом	717
9F	Эконом	717
11A	Эконом	717
11B	Эконом	717
11C	Эконом	717
11D	Эконом	717
11E	Эконом	717
11F	Эконом	717
12A	Эконом	717
12B	Эконом	717
12C	Эконом	717
12D	Эконом	717
12E	Эконом	717
12F	Эконом	717
13A	Эконом	717
13B	Эконом	717
13C	Эконом	717
13D	Эконом	717
13E	Эконом	717
13F	Эконом	717
14A	Эконом	717
14B	Эконом	717
14C	Эконом	717
14D	Эконом	717
14E	Эконом	717
14F	Эконом	717
3A	Эконом	70F
3B	Эконом	70F
3C	Эконом	70F
3D	Эконом	70F
3E	Эконом	70F
3F	Эконом	70F
1A	Бизнес	70F
1B	Бизнес	70F
2A	Бизнес	70F
2B	Бизнес	70F
4A	Эконом	70F
4B	Эконом	70F
4C	Эконом	70F
4D	Эконом	70F
4E	Эконом	70F
4F	Эконом	70F
5A	Эконом	70F
5B	Эконом	70F
5C	Эконом	70F
5D	Эконом	70F
5E	Эконом	70F
5F	Эконом	70F
6A	Эконом	70F
6B	Эконом	70F
6C	Эконом	70F
6D	Эконом	70F
6E	Эконом	70F
6F	Эконом	70F
7A	Эконом	70F
7B	Эконом	70F
7C	Эконом	70F
7D	Эконом	70F
10A	Эконом	70F
10B	Эконом	70F
10C	Эконом	70F
10D	Эконом	70F
10E	Эконом	70F
10F	Эконом	70F
7E	Эконом	70F
7F	Эконом	70F
8A	Эконом	70F
8B	Эконом	70F
8C	Эконом	70F
8D	Эконом	70F
8E	Эконом	70F
8F	Эконом	70F
9A	Эконом	70F
9B	Эконом	70F
9C	Эконом	70F
9D	Эконом	70F
9E	Эконом	70F
9F	Эконом	70F
11A	Эконом	70F
11B	Эконом	70F
11C	Эконом	70F
11D	Эконом	70F
11E	Эконом	70F
11F	Эконом	70F
12A	Эконом	70F
12B	Эконом	70F
12C	Эконом	70F
12D	Эконом	70F
12E	Эконом	70F
12F	Эконом	70F
13A	Эконом	70F
13B	Эконом	70F
13C	Эконом	70F
13D	Эконом	70F
13E	Эконом	70F
13F	Эконом	70F
14A	Эконом	70F
14B	Эконом	70F
14C	Эконом	70F
14D	Эконом	70F
14E	Эконом	70F
14F	Эконом	70F
15C	Эконом	717
15D	Эконом	717
16A	Эконом	717
17B	Эконом	717
19D	Эконом	717
21D	Эконом	717
18C	Эконом	717
20E	Эконом	717
\.


--
-- Data for Name: tickets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tickets (ticket_no, passport_series, passenger_name, price, passenger_surname, passport_number, flight_id, fare_condition, "user") FROM stdin;
9471549493770	7678	Гафиуллова	10300	Каролина	565667	13	Эконом	newuserr
9944202827191	2132	Гафиуллова	5500	Каролина	878978	11	Эконом	caroline
8261942982228	3473	Лоптова	12563	Анна	789739	10	Бизнес	anyalopt
2751006073200	7897	Лоптова	10300	Анна	678687	13	Эконом	anyalopt
3199334703013	7897	Каролина	10300	Гафиуллова	567524	13	Эконом	newuserr
\.


--
-- Name: boarding_passes_boarding_no_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('boarding_passes_boarding_no_seq', 51, true);


--
-- Name: flights_flight_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('flights_flight_id_seq', 6, true);


--
-- Name: prices_price_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('prices_price_id_seq', 27, true);


--
-- Name: aircrafts aircraft_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aircrafts
    ADD CONSTRAINT aircraft_key PRIMARY KEY (aircraft_code);


--
-- Name: airports airport_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY airports
    ADD CONSTRAINT airport_key PRIMARY KEY (airport_code);


--
-- Name: boarding_passes boarding_pass_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY boarding_passes
    ADD CONSTRAINT boarding_pass_key PRIMARY KEY (flight_id, ticket_no);


--
-- Name: boarding_passes boarding_passes_flight_id_boarding_no_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY boarding_passes
    ADD CONSTRAINT boarding_passes_flight_id_boarding_no_key UNIQUE (flight_id, boarding_no);


--
-- Name: boarding_passes boarding_passes_flight_id_seat_no_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY boarding_passes
    ADD CONSTRAINT boarding_passes_flight_id_seat_no_key UNIQUE (flight_id, seat_no);


--
-- Name: prices flight_condition; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT flight_condition UNIQUE (flight_id, fare_condition);


--
-- Name: flights flight_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flights
    ADD CONSTRAINT flight_id UNIQUE (flight_id);


--
-- Name: flights flight_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flights
    ADD CONSTRAINT flight_key PRIMARY KEY (flight_id);


--
-- Name: flights flights_flight_no_departure_time_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flights
    ADD CONSTRAINT flights_flight_no_departure_time_key UNIQUE (flight_no, departure_time);


--
-- Name: prices prices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_pkey PRIMARY KEY (price_id);


--
-- Name: seats seat_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY seats
    ADD CONSTRAINT seat_key PRIMARY KEY (seat_no, aircraft_code);


--
-- Name: tickets ticket_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tickets
    ADD CONSTRAINT ticket_key PRIMARY KEY (ticket_no);


--
-- Name: ix_arr_airp; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_arr_airp ON flights USING btree (arrival_airport);


--
-- Name: ix_dep_airp; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_dep_airp ON flights USING btree (departure_airport);


--
-- Name: ix_flights_aicr; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_flights_aicr ON flights USING btree (aircraft_code);


--
-- Name: seats airc_seats; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY seats
    ADD CONSTRAINT airc_seats FOREIGN KEY (aircraft_code) REFERENCES aircrafts(aircraft_code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: flights arr_airp; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flights
    ADD CONSTRAINT arr_airp FOREIGN KEY (arrival_airport) REFERENCES airports(airport_code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: boarding_passes bp_tic; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY boarding_passes
    ADD CONSTRAINT bp_tic FOREIGN KEY (ticket_no) REFERENCES tickets(ticket_no) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: flights dep_airp; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flights
    ADD CONSTRAINT dep_airp FOREIGN KEY (departure_airport) REFERENCES airports(airport_code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: flights flights_aicr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY flights
    ADD CONSTRAINT flights_aicr FOREIGN KEY (aircraft_code) REFERENCES aircrafts(aircraft_code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: prices prices_flight_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_flight_id_fkey FOREIGN KEY (flight_id) REFERENCES flights(flight_id);


--
-- Name: tickets tick_fl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tickets
    ADD CONSTRAINT tick_fl FOREIGN KEY (flight_id) REFERENCES flights(flight_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: FUNCTION add_user(login text, pswd text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION add_user(login text, pswd text) TO air_admin;
GRANT ALL ON FUNCTION add_user(login text, pswd text) TO air_user;


--
-- Name: FUNCTION available_flights(cityf text, cityt text, ftime date, fare_cond text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION available_flights(cityf text, cityt text, ftime date, fare_cond text) TO air_admin;
GRANT ALL ON FUNCTION available_flights(cityf text, cityt text, ftime date, fare_cond text) TO air_user;


--
-- Name: FUNCTION buy_ticket(nflight_id bigint, npassport_series integer, npassport_number integer, npassenger_name text, npassenger_surname text, nprice integer, nfare_condition text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION buy_ticket(nflight_id bigint, npassport_series integer, npassport_number integer, npassenger_name text, npassenger_surname text, nprice integer, nfare_condition text) TO air_admin;
GRANT ALL ON FUNCTION buy_ticket(nflight_id bigint, npassport_series integer, npassport_number integer, npassenger_name text, npassenger_surname text, nprice integer, nfare_condition text) TO air_user;


--
-- Name: FUNCTION buy_ticket(nflight_id bigint, npassport_series integer, npassport_number integer, npassenger_name text, npassenger_surname character varying, nprice integer, nfare_condition text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION buy_ticket(nflight_id bigint, npassport_series integer, npassport_number integer, npassenger_name text, npassenger_surname character varying, nprice integer, nfare_condition text) TO air_admin;
GRANT ALL ON FUNCTION buy_ticket(nflight_id bigint, npassport_series integer, npassport_number integer, npassenger_name text, npassenger_surname character varying, nprice integer, nfare_condition text) TO air_user;


--
-- Name: FUNCTION check_admin(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION check_admin() TO air_admin;
GRANT ALL ON FUNCTION check_admin() TO air_user;


--
-- Name: FUNCTION flight_status_by_id(fl_id bigint); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION flight_status_by_id(fl_id bigint) TO air_admin;
GRANT ALL ON FUNCTION flight_status_by_id(fl_id bigint) TO air_user;


--
-- Name: FUNCTION flight_status_by_route(cityf text, cityt text, ftime date); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION flight_status_by_route(cityf text, cityt text, ftime date) TO admin;
GRANT ALL ON FUNCTION flight_status_by_route(cityf text, cityt text, ftime date) TO air_user;
GRANT ALL ON FUNCTION flight_status_by_route(cityf text, cityt text, ftime date) TO air_admin;


--
-- Name: FUNCTION generate_ticket_num(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION generate_ticket_num() TO air_admin;
GRANT ALL ON FUNCTION generate_ticket_num() TO air_user;


--
-- Name: FUNCTION reg(_ticket_no character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION reg(_ticket_no character varying) TO admin;
GRANT ALL ON FUNCTION reg(_ticket_no character varying) TO air_user;
GRANT ALL ON FUNCTION reg(_ticket_no character varying) TO air_admin;


--
-- Name: FUNCTION select_user_tickets(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION select_user_tickets() TO air_admin;
GRANT ALL ON FUNCTION select_user_tickets() TO air_user;


--
-- Name: FUNCTION tickets_left(nflight_id bigint, nfare_condition text, OUT n_value integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION tickets_left(nflight_id bigint, nfare_condition text, OUT n_value integer) TO admin;
GRANT ALL ON FUNCTION tickets_left(nflight_id bigint, nfare_condition text, OUT n_value integer) TO air_admin;
GRANT ALL ON FUNCTION tickets_left(nflight_id bigint, nfare_condition text, OUT n_value integer) TO air_user;


--
-- Name: TABLE airports; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE airports TO air_user;


--
-- Name: TABLE flights; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE flights TO caroline;
GRANT SELECT ON TABLE flights TO air_user;


--
-- Name: TABLE tickets; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE tickets TO air_user;


--
-- PostgreSQL database dump complete
--

