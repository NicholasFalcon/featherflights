<?php

namespace app\models;

use Yii;
use yii\base\Model;

class SearchForm extends Tickets
{
    public $townWhere;
    public $townFrom;
    public $dateWhere;
    public $dateFrom;

    public function rules()
    {
        return [
        [['townWhere', 'townFrom', 'dateWhere'], 'required', 'message'=>'Не все основные поля введены'],
        ];
    }
    
    public function Search()
    {
        return $this->townWhere;
    }
}

?>
