<?php

namespace app\models;

class User extends Customers implements \yii\web\IdentityInterface
{


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($Customer_ID)
    {
        return static::findOne($Customer_ID);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * {@inheritdoc}
     */
    public function getID()
    {
        return $this->customer_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $pswd password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePswd($password)
    {
        return $this->pswd === md5($password);
    }
}
