<?php

namespace app\models;

use Yii;
use yii\base\Model;

class SignupForm extends User
{

    public $Name;
    public $Surname;
    public $DateOfBirth;
    public $email;
    public $password;
    public $verifyCode;

    public function rules()
    {
        return [[['Name', 'Surname', 'DateOfBirth', 'email', 'password'], 'required', 'message' => 'Это поле не может быть пустым'],
        ['email', 'email', 'message' => 'Введенные данные не соответствуют адресу электронной почты'],
        ['email', 'trim'],
        ['email', 'unique', 'targetAttribute' => 'email', 'message' => 'Эта электронная почта уже зарегистрирована'],
        ['password', 'string', 'min' => 6, 'tooShort' => 'Пароль должен содержать не менее 6 символов'],
        ['verifyCode', 'captcha', 'message' => 'Код проверки не совпадает'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new Customers();
        $user->name = $this->Name;
        $user->surname = $this->Surname;
        $user->email = $this->email;
        $user->pswd = md5($this->password);
        $user->date_of_birth = $this->DateOfBirth;
        return $user;
    }
}
