<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Flights".
 *
 * @property int $Flight_id
 * @property string $Flight_no
 * @property string $Departure_time
 * @property string $Arrival_time
 * @property string $Status
 * @property string $Arrival_airport
 * @property string $Departure_airport
 * @property string $Aircraft_code
 *
 * @property Airports $arrivalAirport
 * @property Airports $departureAirport
 * @property Aircrafts $aircraftCode
 * @property Tickets[] $tickets
 */
class Flights extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flight_id', 'flight_no', 'status', 'arrival_airport', 'departure_airport', 'aircraft_code'], 'required'],
            [['flight_id'], 'integer'],
            [['departure_time', 'Arrival_time'], 'safe'],
            [['status'], 'string'],
            [['flight_no'], 'string', 'max' => 6],
            [['arrival_airport', 'departure_airport', 'aircraft_code'], 'string', 'max' => 3],
            [['flight_id'], 'unique'],
            [['arrival_airport'], 'exist', 'skipOnError' => true, 'targetClass' => Airports::className(), 'targetAttribute' => ['arrival_airport' => 'airport_code']],
            [['departure_airport'], 'exist', 'skipOnError' => true, 'targetClass' => Airports::className(), 'targetAttribute' => ['departure_airport' => 'airport_code']],
            [['aircraft_code'], 'exist', 'skipOnError' => true, 'targetClass' => Aircrafts::className(), 'targetAttribute' => ['aircraft_code' => 'aircraft_code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Flight_id' => 'Flight ID',
            'Flight_no' => 'Flight No',
            'Departure_time' => 'Departure Time',
            'Arrival_time' => 'Arrival Time',
            'Status' => 'Status',
            'Arrival_airport' => 'Arrival Airport',
            'Departure_airport' => 'Departure Airport',
            'Aircraft_code' => 'Aircraft Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrivalAirport()
    {
        return $this->hasOne(Airports::className(), ['Airport_code' => 'Arrival_airport']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartureAirport()
    {
        return $this->hasOne(Airports::className(), ['Airport_code' => 'Departure_airport']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAircraftCode()
    {
        return $this->hasOne(Aircrafts::className(), ['Aircraft_code' => 'Aircraft_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Tickets::className(), ['Flight_id' => 'Flight_id']);
    }
}
