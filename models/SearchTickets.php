<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tickets;

/**
 * SearchTickets represents the model behind the search form of `app\models\Tickets`.
 */
class SearchTickets extends Tickets
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_no', 'passenger_name', 'passenger_surname'], 'safe'],
            [['flight_id', 'customer_id', 'passport_series', 'passport_number'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tickets::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'flight_id' => $this->flight_id,
            'customer_id' => $this->customer_id,
            'passport_series' => $this->passport_series,
            'passport_number' => $this->passport_number,
        ]);

        $query->andFilterWhere(['like', 'ticket_no', $this->ticket_no])
            ->andFilterWhere(['like', 'passenger_name', $this->passenger_name])
            ->andFilterWhere(['like', 'passenger_surname', $this->passenger_surname]);

        return $dataProvider;
    }
}
