<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property int $customer_id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $pswd
 * @property string $date_of_birth
 * @property string $access_token
 * @property string $auth_key
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'email', 'pswd'], 'required'],
            [['name', 'surname', 'email', 'pswd', 'access_token', 'auth_key'], 'string'],
            [['date_of_birth'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'email' => 'Email',
            'pswd' => 'Pswd',
            'date_of_birth' => 'Date Of Birth',
            'access_token' => 'Access Token',
            'auth_key' => 'Auth Key',
        ];
    }
}
