<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tickets".
 *
 * @property string $ticket_no
 * @property int $passport_series
 * @property string $passenger_name
 * @property int $price
 * @property string $passenger_surname
 * @property int $passport_number
 * @property string $flight_id
 * @property string $fare_condition
 * @property string $user
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_no', 'passport_series', 'passenger_name', 'price', 'passenger_surname', 'passport_number', 'flight_id'], 'required'],
            [['passport_series', 'price', 'passport_number', 'flight_id'], 'default', 'value' => null],
            [['passport_series', 'price', 'passport_number', 'flight_id'], 'integer'],
            [['passenger_name', 'passenger_surname', 'fare_condition', 'user'], 'string'],
            [['ticket_no'], 'string', 'max' => 13],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'customer_id']],
            [['flight_id'], 'exist', 'skipOnError' => true, 'targetClass' => Flights::className(), 'targetAttribute' => ['flight_id' => 'flight_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ticket_no' => 'Ticket No',
            'passport_series' => 'Passport Series',
            'passenger_name' => 'Passenger Name',
            'price' => 'Price',
            'passenger_surname' => 'Passenger Surname',
            'passport_number' => 'Passport Number',
            'flight_id' => 'Flight ID',
            'fare_condition' => 'Fare Condition',
            'user' => 'User',
        ];
    }
}
