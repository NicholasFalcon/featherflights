<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "airports".
 *
 * @property string $airport_code
 * @property string $airport_name
 * @property string $city
 * @property string $timezone
 * @property string $country
 */
class Airports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'airports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['airport_code', 'airport_name', 'city', 'timezone', 'country'], 'required'],
            [['airport_name', 'city', 'timezone', 'country'], 'string'],
            [['airport_code'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'airport_code' => 'Airport Code',
            'airport_name' => 'Airport Name',
            'city' => 'City',
            'timezone' => 'Timezone',
            'country' => 'Country',
        ];
    }
}
