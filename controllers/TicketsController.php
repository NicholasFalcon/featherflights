<?php

namespace app\controllers;

use Yii;
use app\models\Tickets;
use app\models\SearchTickets;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketsController implements the CRUD actions for Tickets model.
 */
class TicketsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tickets models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchTickets();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tickets model.
     * @param string $Ticket_no
     * @param integer $Flight_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($Ticket_no, $Flight_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($Ticket_no, $Flight_id),
        ]);
    }

    /**
     * Creates a new Tickets model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tickets();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Ticket_no' => $model->Ticket_no, 'Flight_id' => $model->Flight_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tickets model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Ticket_no
     * @param integer $Flight_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($Ticket_no, $Flight_id)
    {
        $model = $this->findModel($Ticket_no, $Flight_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Ticket_no' => $model->Ticket_no, 'Flight_id' => $model->Flight_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tickets model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Ticket_no
     * @param integer $Flight_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($Ticket_no, $Flight_id)
    {
        $this->findModel($Ticket_no, $Flight_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tickets model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Ticket_no
     * @param integer $Flight_id
     * @return Tickets the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Ticket_no, $Flight_id)
    {
        if (($model = Tickets::findOne(['Ticket_no' => $Ticket_no, 'Flight_id' => $Flight_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
